package cl.uai.oop;

public enum PlaceType {
    MUSEUM, THEATRE, PALACE, GARDEN, STADIUM
}