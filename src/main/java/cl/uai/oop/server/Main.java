/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uai.oop.server;

import cl.uai.oop.Place;
import cl.uai.oop.Places;
import cl.uai.oop.PlaceComparator;
import cl.uai.oop.PlaceLocation;
import cl.uai.oop.Utils;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author gohucan
 */
public class Main {

    private static Object lock = new Object();

    public static void main(String args[]) {
        try {
            Place place = new Place();
            place.setName("Demo place");
            place.setDescription("A nice place");
            place.setLocation(new PlaceLocation(0.1,0.1));
            place.setOpeningHour(new Date());
            place.setClosingHour(new Date());
            Places.add(place);
            int portNumber = Integer.parseInt(args[0]);
            ServerSocket serverSocket = new ServerSocket(portNumber);
            while (true) {
                Socket clientSocket = null;
                try {
                    clientSocket = serverSocket.accept();
                    ServerThread st = new ServerThread(clientSocket);
                    st.start();
                } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static class ServerThread extends Thread {

        private Socket clientSocket;

        public ServerThread(Socket clientSocket) {
            this.clientSocket = clientSocket;
        }

        @Override
        public void run() {
            try {
                InputStream is = clientSocket.getInputStream();
                OutputStream os = clientSocket.getOutputStream();
                DataInputStream in = new DataInputStream(is);
                DataOutputStream out = new DataOutputStream(os);
                while(true) {
                    String command = in.readUTF();
                    String[] parameters = command.split(";");
                    int option = Integer.parseInt(parameters[0]);
                    if (option == 1) {
                        PlaceLocation pl = new PlaceLocation(Double.parseDouble(parameters[1]), Double.parseDouble(parameters[2]));
                        try {
                            //TODO: obtener la lista de lugares más cercanos
                            synchronized(lock) {
                                List<Place> places = Places.get();
                                out.writeInt(places.size());
                                Collections.sort(places, new PlaceComparator(pl));
                                //TODO: escribir la lista en el socket
                                for(Place p: places) {
                                    out.writeUTF(String.format("%s;%f", p.getName(), Utils.distance(p.getLocation(), pl)));
                                }
                            }
                        } catch (Exception e) {
                            out.writeUTF("An error has ocurred");
                        }
                    } else if (option == 2) {
                        String placeName = parameters[1];
                        try {
                            //TODO: obtener el lugar (si existe)
                            List<Place> places = Places.get();
                            Place fake = new Place();
                            fake.setName(placeName);
                            int index = places.indexOf(fake);
                            //TODO: escribir el lugar en el socket
                            Place place = places.get(index);
                            out.writeUTF(String.format("%s;%s;%s;%s;%s;%s", place.getName(), place.getDescription(), place.getType(), place.getLocation(), place.getOpeningHour(), place.getClosingHour()));
                        } catch (java.lang.IndexOutOfBoundsException iofbe) {
                            out.writeUTF("Place not found");
                        } catch (Exception e) {
                            out.writeUTF("An error has ocurred");
                        }
                    }
                }
            } catch (Exception ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

}
