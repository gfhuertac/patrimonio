/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uai.oop.client;

import cl.uai.oop.Place;
import cl.uai.oop.PlaceLocation;
import com.maxmind.geoip2.exception.GeoIp2Exception;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author gohucan
 */
public class Main {

    public static void main(String[] args) {
        try {
            String hostName = args[0];
            int portNumber = Integer.parseInt(args[1]);
            Socket mySocket = new Socket(hostName, portNumber);
            InputStream is = mySocket.getInputStream();
            OutputStream os = mySocket.getOutputStream();
            DataOutputStream dos = new DataOutputStream(os);
            DataInputStream dis = new DataInputStream(is);
            Scanner myScanner = new Scanner(System.in);
            myScanner.useDelimiter(System.getProperty("line.separator"));
            int option = -1;
            while(true) {
                System.out.println("Bienvenido. Ingrese la opción deseada.");
                System.out.println("1 para listar los lugares cercanos");
                System.out.println("2 para encontrar información de un lugar");
                System.out.println("0 para salir");
                option = myScanner.nextInt();
                if (option == 0)
                    break;
                else if (option == 1) {
                    PlaceLocation pl;
                    try {
                        pl  = PlaceLocation.currentLocation();
                    } catch(Exception e) {
                        pl = new PlaceLocation(0.0,0.0);
                    }
                    dos.writeUTF(String.format("1;%s", pl.toString()));
                    dos.flush();
                    //TODO: leer respuesta del servidor y mostrarla en pantalla
                    int nbrPlaces = dis.readInt();
                    for(int i=0; i<nbrPlaces; i++) {
                        String place = dis.readUTF();
                        System.out.println(place.replaceAll(";", " a "));
                    }
                } else if (option == 2) {
                    System.out.println("Ingrese nombre del lugar a consultar:");
                    String place = myScanner.next();
                    //TODO: enviar info al servidor
                    dos.writeUTF(String.format("2;%s", place));
                    dos.flush();
                    //TODO: leer respuesta del servidor y mostrarla en pantalla
                    String reply = dis.readUTF();
                    String[] description = reply.split(";");
                    for(String s: description) {
                        System.out.println(s);
                    }
                } else {
                    System.out.println("Opción inválida");
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}