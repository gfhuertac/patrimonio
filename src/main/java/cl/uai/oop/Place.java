package cl.uai.oop;

import java.util.Date;

public final class Place {

    protected String name;
    protected String description;
    protected PlaceType type;
    protected PlaceLocation location;
    protected Date openingHour;
    protected Date closingHour;

    public String getName() {
        return this.name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String value) {
        this.description = value;
    }

    public PlaceType getType() {
        return this.type;
    }

    public void setType(PlaceType value) {
        this.type = value;
    }

    public PlaceLocation getLocation() {
        return this.location;
    }

    public void setLocation(PlaceLocation value) {
        this.location = value;
    }

    public Date getOpeningHour() {
        return this.openingHour;
    }

    public void setOpeningHour(Date value) {
        this.openingHour = value;
    }

    public Date getClosingHour() {
        return this.closingHour;
    }

    public void setClosingHour(Date value) {
        this.closingHour = value;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof String) {
            return ((String)o).equals(this.getName());
        } else if (o instanceof Place) {
            return ((Place)o).getName().equals(this.getName());
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return this.getName();
    }
}