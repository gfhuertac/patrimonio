package cl.uai.oop;

import java.util.Comparator;

public final class PlaceComparator implements Comparator<Place> {

    private final PlaceLocation anchor;

    public PlaceComparator(PlaceLocation anchor) {
        this.anchor = anchor;
    }

    public int compare(Place o1, Place o2) {
        double d1 = Utils.distance(o1.getLocation(), this.anchor);
        double d2 = Utils.distance(o2.getLocation(), this.anchor);

        return (int)(d1 - d2);
    }

}