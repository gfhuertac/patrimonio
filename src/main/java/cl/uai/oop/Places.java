package cl.uai.oop;

import java.util.List;
import java.util.ArrayList;

public final class Places {

    private static final List<Place> places = new ArrayList<>();

    public static List<Place> get() {
        return places;
    }

    public static void add(Place place) {
        if (places.indexOf(place) < 0) {
            places.add(place);
        }
    }

}